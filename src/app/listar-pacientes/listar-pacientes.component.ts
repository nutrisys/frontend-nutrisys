import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PacientesService } from '../services/pacientes.service';
import { NotificacionService } from '../services/notificacion.service';

@Component({
  selector: 'app-listar-pacientes',
  templateUrl: './listar-pacientes.component.html',
  styleUrls: ['./listar-pacientes.component.css']
})
export class ListarPacientesComponent implements OnInit {

  pacientes: any[] = [];
  loading = false;


  constructor(
    private _router: Router,
    private _pacientesServices: PacientesService,
    private _notificacionService: NotificacionService
  ) { }

  ngOnInit(): void {
    this.listarPacientes();
  }

  async listarPacientes() {
    this.loading = true;
    const response = await this._pacientesServices.listarPacientes();
    console.log(response);
    this.loading = false;
    if (response["status"] === "success" && response["code"] === "200") {
      // this._notificacionService.showNotification('success', 'top', 'right', response["message"]);
      this.pacientes = response["data"];
      console.log(response["message"]);
    } else {
      this._notificacionService.showNotification('danger', 'top', 'right', response["message"]);

      console.log(response["message"]);
    }
  }
  
  async verPaciente(doc: any) {
    this._router.navigate(['control-paciente', doc])
  }
  
  crearPaciente(){
    this._router.navigate(['/registrar-paciente']);
  }

}
