import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { LoadingModule } from '../shared/loading/loading.module';



@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule,
    LoadingModule
  ],
})
export class PagesModule { }
