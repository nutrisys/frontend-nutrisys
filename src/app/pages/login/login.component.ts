import { Component, OnInit, ElementRef, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { UsuarioService } from '../../services/usuario.service';
import { Login } from '../../models/login';
import { NotificacionService } from '../../services/notificacion.service';

declare var $: any;

@Component({
    selector: 'app-login-cmp',
    templateUrl: './login.component.html'
})

export class LoginComponent implements OnInit, OnDestroy {

    public login: Login;
    public identity: any;
    public token: any;
    public hide = true;


    test: Date = new Date();
    private toggleButton: any;
    private sidebarVisible: boolean;
    private nativeElement: Node;
    public loading = false;

    constructor(private element: ElementRef,
        private _router: Router,
        private _usuarioService: UsuarioService,
        private _notificacionService: NotificacionService) {

        this.nativeElement = element.nativeElement;
        this.sidebarVisible = false;
        this.login = new Login('', '');


    }

    ngOnInit() {
        var navbar: HTMLElement = this.element.nativeElement;
        this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];
        const body = document.getElementsByTagName('body')[0];
        body.classList.add('login-page');
        body.classList.add('off-canvas-sidebar');
        const card = document.getElementsByClassName('card')[0];
        setTimeout(function () {
            // after 1000 ms we add the class animated to the login/register card
            card.classList.remove('card-hidden');
        }, 700);
    }
    sidebarToggle() {
        var toggleButton = this.toggleButton;
        var body = document.getElementsByTagName('body')[0];
        var sidebar = document.getElementsByClassName('navbar-collapse')[0];
        if (this.sidebarVisible == false) {
            setTimeout(function () {
                toggleButton.classList.add('toggled');
            }, 500);
            body.classList.add('nav-open');
            this.sidebarVisible = true;
        } else {
            this.toggleButton.classList.remove('toggled');
            this.sidebarVisible = false;
            body.classList.remove('nav-open');
        }
    }
    ngOnDestroy() {
        const body = document.getElementsByTagName('body')[0];
        body.classList.remove('login-page');
        body.classList.remove('off-canvas-sidebar');
    }

    async iniciarSesion(loginForm) {
        this.loading = true;
       
        const response: any = await this._usuarioService.iniciarSesion(this.login);

        if (response['status'] == "success") {

            this.identity = response['data'];
            this._notificacionService.showNotification('success', 'top', 'right', response["message"]);

            const responseDatosLoginToken = await this._usuarioService.iniciarSesion(this.login, true);

            this.token = responseDatosLoginToken;

            console.log(this.identity);
            

            // Guardar token en el localstorage
            localStorage.setItem("token", this.token);
            //localStorage.setItem("SesionEmpresa", this.login.sessionEmpresa);
            localStorage.setItem('identity', JSON.stringify(this.identity));
            this.loading = false;

            this._router.navigate(['/dashboard']);
        } else {
            this.loading = false;
            this._notificacionService.showNotification('danger', 'top', 'right', response['message']);
        }
    }

}
