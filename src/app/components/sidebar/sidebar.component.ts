import { Component, OnInit } from "@angular/core";
import { UsuarioService } from "app/services/usuario.service";
import { storage } from "googleapis/build/src/apis/storage";

declare const $: any;
declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}
export const ROUTES_ADMIN: RouteInfo[] = [
  { path: "/dashboard", title: "Dashboard", icon: "dashboard", class: "" },
  { path: "/usuarios", title: "Usuarios", icon: "person", class: "" },
  { path: "/pacientes", title: "Pacientes", icon: "content_paste", class: "" },
];
export const ROUTES_MEDICO: RouteInfo[] = [
  { path: "/dashboard", title: "Dashboard", icon: "dashboard", class: "" },
  { path: "/pacientes", title: "Pacientes", icon: "content_paste", class: "" },
];
@Component({
  selector: "app-sidebar",
  templateUrl: "./sidebar.component.html",
  styleUrls: ["./sidebar.component.css"],
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor(private _usuarioService: UsuarioService) {}

  ngOnInit() {
    const identity = JSON.parse(localStorage.getItem("identity"));

    if (identity.rol === "administrador") {
      this.menuItems = ROUTES_ADMIN.filter((menuItem) => menuItem);
    } else if (identity.rol === "medico") {
      this.menuItems = ROUTES_MEDICO.filter((menuItem) => menuItem);
    }
  }
  isMobileMenu() {
    if ($(window).width() > 991) {
      return false;
    }
    return true;
  }

  cerrarSesion() {
    this._usuarioService.cerrarSesion();
  }
}
