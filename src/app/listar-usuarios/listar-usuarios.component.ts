import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RegistroUsuariosService } from '../services/registro-usuarios.service';
import { NotificacionService } from '../services/notificacion.service';

@Component({
  selector: 'app-listar-usuarios',
  templateUrl: './listar-usuarios.component.html',
  styleUrls: ['./listar-usuarios.component.css']
})
export class ListarUsuariosComponent implements OnInit {

  usuarios: any[] = [];
  loading = false;


  constructor(
    private _router: Router,
    private _registroUsuarioService: RegistroUsuariosService,
    private _notificacionService: NotificacionService
  ) { }

  ngOnInit(): void {

    this.listarUsuarios();
  }

  async listarUsuarios() {
    this.loading = true;
    const response = await this._registroUsuarioService.listarUsuarios();
    this.loading = false;
    console.log(response);
    if (response["status"] === "success" && response["code"] === "200") {
      // this._notificacionService.showNotification('success', 'top', 'right', response["message"]);
      this.usuarios = response["data"];
      console.log(response["message"]);
    } else {
      this._notificacionService.showNotification('danger', 'top', 'right', response["message"]);

      console.log(response["message"]);
    }
  }

  crearUsuario(){
    this._router.navigate(['/registrar-usuario']);
  }

}
