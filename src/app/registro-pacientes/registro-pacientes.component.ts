import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
} from "@angular/forms";
import {
  DatosPersonales,
  Antropometria,
  Comorbilidad,
  ComorbilidadData,
  PerfilBioquimico,
  Tratamiento,
} from "../models/pacientes";
import { PacientesService } from "../services/pacientes.service";
import { NotificacionService } from "../services/notificacion.service";
import swal from "sweetalert2";
import { ActivatedRoute, Router } from "@angular/router";
import { environment } from "environments/environment";

@Component({
  selector: "app-registro-pacientes",
  templateUrl: "./registro-pacientes.component.html",
  styleUrls: ["./registro-pacientes.component.css"],
})
export class RegistroPacientesComponent implements OnInit {
  datosPersonalesForm: FormGroup;
  antropometriaForm: FormGroup;
  comorbilidadForm: FormGroup;
  perfilBioquimicoForm: FormGroup;
  indicesForm: FormGroup;
  tratamientoForm: FormGroup;
  isEditable = true;

  idPerfilBioquimico: any;
  loading = false;
  idControl: any;
  indices: any;
  idAntropometria: any;
  imgCorpulencia = 10;

  idPaciente: any;

  registroControl = true;

  constructor(
    private _pacienteServices: PacientesService,
    private _notificacionService: NotificacionService,
    private _router: Router,
    private _route: ActivatedRoute
  ) {
    console.log(this.idPaciente);
    this.idPaciente = this._route.snapshot.paramMap.get("idPaciente");
  }

  ngOnInit() {
    if (this.idPaciente == undefined) {
      this.registroControl = true;
      this.datosPersonalesForm = new FormGroup({
        identificacion: new FormControl(null, Validators.required),
        nombres: new FormControl(null, Validators.required),
        apellidos: new FormControl(null, Validators.required),
        genero: new FormControl(null, Validators.required),
        // edad: new FormControl(null, Validators.required),
        email: new FormControl(null, Validators.required),
        direccion: new FormControl(null, Validators.required),
        telefono: new FormControl(null, Validators.required),
        fechaNacimiento: new FormControl(null, Validators.required),
      });

      this.antropometriaForm = new FormGroup({
        altura: new FormControl(null, Validators.required),
        peso: new FormControl(null, Validators.required),
        radioCaderaCintura: new FormControl(null, Validators.required),
        circunferenciaMediaCuello: new FormControl(null, Validators.required),
        circunferenciaMediaAbdomen: new FormControl(null, Validators.required),
        circunferenciaMediaCadera: new FormControl(null, Validators.required),
      });

      this.comorbilidadForm = new FormGroup({
        hipertension: new FormControl(null, Validators.required),
        diabetes: new FormControl(null, Validators.required),
        infartoMediocardioPrevio: new FormControl(null, Validators.required),
        enfrenalCronica: new FormControl(null, Validators.required),
        enfRenalCronicaDialisis: new FormControl(null, Validators.required),
        enfCerebrovascular: new FormControl(null, Validators.required),
        epoc: new FormControl(null, Validators.required),
        tabaquismo: new FormControl(null, Validators.required),
        sahos: new FormControl(null, Validators.required),
        vih: new FormControl(null, Validators.required),
        neoplasiaActiva: new FormControl(null, Validators.required),
        enfAutoinmune: new FormControl(null, Validators.required),
      });

      this.perfilBioquimicoForm = new FormGroup({
        glucemia: new FormControl(null, Validators.required),
        insulina: new FormControl(null, Validators.required),
        homaIr: new FormControl(null, Validators.required),
        leptina: new FormControl(null, Validators.required),
      });

      this.indicesForm = new FormGroup({
        imc: new FormControl({ value: "", disabled: true }),
        imgrasa: new FormControl({ value: "", disabled: true }),
        masagrasacorporal: new FormControl({ value: "", disabled: true }),
        imlibredegrasa: new FormControl({ value: "", disabled: true }),
        masalibredegrasa: new FormControl({ value: "", disabled: true }),
        masamusculoesquelitica: new FormControl({ value: "", disabled: true }),
      });

      this.tratamientoForm = new FormGroup({
        tratamiento: new FormControl(null),
        observacion: new FormControl(null),
      });

      // Setear Valores

      // this.perfilBioquimicoForm.get("glucemia").setValue("1");
      // this.perfilBioquimicoForm.get("insulina").setValue("2");
      // this.perfilBioquimicoForm.get("homaIr").setValue("3");
      // this.perfilBioquimicoForm.get("leptina").setValue("4");

      // this.datosPersonalesForm.get("identificacion").setValue("12356789");
      // this.datosPersonalesForm.get("nombres").setValue("María");
      // this.datosPersonalesForm.get("apellidos").setValue("Lopez");
      // this.datosPersonalesForm.get("genero").setValue("F");
      // // this.datosPersonalesForm.get('edad').setValue('25');
      // this.datosPersonalesForm.get("email").setValue("marialopez@correo.com");
      // this.datosPersonalesForm.get("direccion").setValue("Cra 123");
      // this.datosPersonalesForm.get("telefono").setValue("7205896");
      // this.datosPersonalesForm.get("fechaNacimiento").setValue("01/08/2020");

      // this.antropometriaForm.get("altura").setValue("1.53");
      // this.antropometriaForm.get("peso").setValue("55");
      // this.antropometriaForm.get("radioCaderaCintura").setValue("8.2");
      // this.antropometriaForm.get("circunferenciaMediaCuello").setValue("15");
      // this.antropometriaForm.get("circunferenciaMediaAbdomen").setValue("16");
      // this.antropometriaForm.get("circunferenciaMediaCadera").setValue("18");

      // this.comorbilidadForm.get("hipertension").setValue("NO");
      // this.comorbilidadForm.get("diabetes").setValue("NO");
      // this.comorbilidadForm.get("infartoMediocardioPrevio").setValue("NO");
      // this.comorbilidadForm.get("enfrenalCronica").setValue("NO");
      // this.comorbilidadForm.get("enfRenalCronicaDialisis").setValue("NO");
      // this.comorbilidadForm.get("enfCerebrovascular").setValue("NO");
      // this.comorbilidadForm.get("epoc").setValue("NO");
      // this.comorbilidadForm.get("tabaquismo").setValue("NO");
      // this.comorbilidadForm.get("sahos").setValue("NO");
      // this.comorbilidadForm.get("vih").setValue("NO");
      // this.comorbilidadForm.get("neoplasiaActiva").setValue("NO");
      // this.comorbilidadForm.get("enfAutoinmune").setValue("NO");
      
    } else {

      this.onSubmitControl(this.idPaciente);

      this.registroControl = false;
      console.log("Control para cliente existente");

      this.antropometriaForm = new FormGroup({
        altura: new FormControl(null, Validators.required),
        peso: new FormControl(null, Validators.required),
        radioCaderaCintura: new FormControl(null, Validators.required),
        circunferenciaMediaCuello: new FormControl(null, Validators.required),
        circunferenciaMediaAbdomen: new FormControl(null, Validators.required),
        circunferenciaMediaCadera: new FormControl(null, Validators.required),
      });

      this.comorbilidadForm = new FormGroup({
        hipertension: new FormControl(null, Validators.required),
        diabetes: new FormControl(null, Validators.required),
        infartoMediocardioPrevio: new FormControl(null, Validators.required),
        enfrenalCronica: new FormControl(null, Validators.required),
        enfRenalCronicaDialisis: new FormControl(null, Validators.required),
        enfCerebrovascular: new FormControl(null, Validators.required),
        epoc: new FormControl(null, Validators.required),
        tabaquismo: new FormControl(null, Validators.required),
        sahos: new FormControl(null, Validators.required),
        vih: new FormControl(null, Validators.required),
        neoplasiaActiva: new FormControl(null, Validators.required),
        enfAutoinmune: new FormControl(null, Validators.required),
      });

      this.perfilBioquimicoForm = new FormGroup({
        glucemia: new FormControl(null, Validators.required),
        insulina: new FormControl(null, Validators.required),
        homaIr: new FormControl(null, Validators.required),
        leptina: new FormControl(null, Validators.required),
      });

      this.indicesForm = new FormGroup({
        imc: new FormControl({ value: "", disabled: true }),
        imgrasa: new FormControl({ value: "", disabled: true }),
        masagrasacorporal: new FormControl({ value: "", disabled: true }),
        imlibredegrasa: new FormControl({ value: "", disabled: true }),
        masalibredegrasa: new FormControl({ value: "", disabled: true }),
        masamusculoesquelitica: new FormControl({ value: "", disabled: true }),
      });

      this.tratamientoForm = new FormGroup({
        tratamiento: new FormControl(null),
        observacion: new FormControl(null),
      });

      // Setear Valores

      // this.perfilBioquimicoForm.get("glucemia").setValue("1");
      // this.perfilBioquimicoForm.get("insulina").setValue("2");
      // this.perfilBioquimicoForm.get("homaIr").setValue("3");
      // this.perfilBioquimicoForm.get("leptina").setValue("4");

      // this.antropometriaForm.get("altura").setValue("1.53");
      // this.antropometriaForm.get("peso").setValue("55");
      // this.antropometriaForm.get("radioCaderaCintura").setValue("8.2");
      // this.antropometriaForm.get("circunferenciaMediaCuello").setValue("15");
      // this.antropometriaForm.get("circunferenciaMediaAbdomen").setValue("16");
      // this.antropometriaForm.get("circunferenciaMediaCadera").setValue("18");

      // this.comorbilidadForm.get("hipertension").setValue("NO");
      // this.comorbilidadForm.get("diabetes").setValue("NO");
      // this.comorbilidadForm.get("infartoMediocardioPrevio").setValue("NO");
      // this.comorbilidadForm.get("enfrenalCronica").setValue("NO");
      // this.comorbilidadForm.get("enfRenalCronicaDialisis").setValue("NO");
      // this.comorbilidadForm.get("enfCerebrovascular").setValue("NO");
      // this.comorbilidadForm.get("epoc").setValue("NO");
      // this.comorbilidadForm.get("tabaquismo").setValue("NO");
      // this.comorbilidadForm.get("sahos").setValue("NO");
      // this.comorbilidadForm.get("vih").setValue("NO");
      // this.comorbilidadForm.get("neoplasiaActiva").setValue("NO");
      // this.comorbilidadForm.get("enfAutoinmune").setValue("NO");
    }
  }

  async onSubmitGuardarDatosPersonalesPaciente() {
    this.loading = true;

    let paciente = new DatosPersonales(
      this.datosPersonalesForm.value.identificacion,
      this.datosPersonalesForm.value.nombres,
      this.datosPersonalesForm.value.apellidos,
      this.datosPersonalesForm.value.genero,
      // this.datosPersonalesForm.value.edad,
      this.datosPersonalesForm.value.email,
      this.datosPersonalesForm.value.direccion,
      this.datosPersonalesForm.value.telefono,
      this.datosPersonalesForm.value.fechaNacimiento
    );
    console.log(paciente);

    const response = await this._pacienteServices.crearDatosPersonalesPaciente(
      paciente
    );
    console.log(response);
    this.loading = false;

    if (response["status"] === "success" && response["code"] === "200") {
      this.idControl = response["data"].idControl;
      localStorage.setItem("idControl", this.idControl);

      this._notificacionService.showNotification(
        "success",
        "top",
        "right",
        response["message"]
      );
    } else {
      console.log(response["message"]);
      this._notificacionService.showNotification(
        "danger",
        "top",
        "right",
        response["message"]
      );
    }
  }

  async onSubmitAntopometria() {
    const idControl = localStorage.getItem("idControl");

    this.loading = true;

    let antopometria = new Antropometria(
      this.antropometriaForm.value.altura,
      this.antropometriaForm.value.peso,
      this.antropometriaForm.value.radioCaderaCintura,
      this.antropometriaForm.value.circunferenciaMediaCuello,
      this.antropometriaForm.value.circunferenciaMediaAbdomen,
      this.antropometriaForm.value.circunferenciaMediaCadera,
      idControl
    );
    console.log(antopometria);

    const response = await this._pacienteServices.crearAntopometriaPaciente(
      antopometria
    );
    console.log(response);
    this.loading = false;

    if (response["status"] === "success" && response["code"] === "200") {
      this.idAntropometria = response["data"].antropometria.id;
      console.log(this.idAntropometria);
      localStorage.setItem("idAntropometria", this.idAntropometria);
      console.log(response["message"]);
      this._notificacionService.showNotification(
        "success",
        "top",
        "right",
        response["message"]
      );
    } else {
      console.log(response["message"]);
      this._notificacionService.showNotification(
        "danger",
        "top",
        "right",
        response["message"]
      );
    }
  }

  async onSubmitPerfilBioquimico() {
    const idControl = localStorage.getItem("idControl");

    this.loading = true;

    let insulina = this.perfilBioquimicoForm.value.insulina;
    let glucemia = this.perfilBioquimicoForm.value.glucemia;
    let homaIr = insulina * glucemia;

    let perfilBioquimico = new PerfilBioquimico(
      this.perfilBioquimicoForm.value.glucemia,
      this.perfilBioquimicoForm.value.insulina,
      homaIr.toString(),
      this.perfilBioquimicoForm.value.leptina,
      idControl
    );
    console.log(perfilBioquimico);

    const response = await this._pacienteServices.crearPerfilBioquimicoPaciente(
      perfilBioquimico
    );

    this.loading = false;
    console.log(response);
    if (response["status"] === "success" && response["code"] === "200") {
      console.log(response["message"]);
      this.idPerfilBioquimico = response["data"].id;
      localStorage.setItem("idPerfilBioquimico", this.idPerfilBioquimico);

      this._notificacionService.showNotification(
        "success",
        "top",
        "right",
        response["message"]
      );
    } else {
      console.log(response["message"]);
      this._notificacionService.showNotification(
        "danger",
        "top",
        "right",
        response["message"]
      );
    }
  }

  async onSubmitComorbilidad() {
    const idControl = localStorage.getItem("idControl");
    const idPerfilBioquimico = localStorage.getItem("idPerfilBioquimico");

    this.loading = true;

    let comorbilidad = new Comorbilidad(
      this.comorbilidadForm.value.hipertension,
      this.comorbilidadForm.value.diabetes,
      this.comorbilidadForm.value.infartoMediocardioPrevio,
      this.comorbilidadForm.value.enfrenalCronica,
      this.comorbilidadForm.value.enfRenalCronicaDialisis,
      this.comorbilidadForm.value.enfCerebrovascular,
      this.comorbilidadForm.value.epoc,
      this.comorbilidadForm.value.tabaquismo,
      this.comorbilidadForm.value.sahos,
      this.comorbilidadForm.value.vih,
      this.comorbilidadForm.value.neoplasiaActiva,
      this.comorbilidadForm.value.enfAutoinmune
    );
    console.log(comorbilidad);

    let comorbilidadData = new ComorbilidadData(
      JSON.stringify(comorbilidad),
      idPerfilBioquimico,
      idControl
    );

    const response = await this._pacienteServices.crearComorbilidadPaciente(
      comorbilidadData
    );
    this.loading = false;

    console.log(response);
    if (response["status"] === "success" && response["code"] === "200") {
      console.log(response["message"]);
      this._notificacionService.showNotification(
        "success",
        "top",
        "right",
        response["message"]
      );
    } else {
      console.log(response["message"]);
      this._notificacionService.showNotification(
        "danger",
        "top",
        "right",
        response["message"]
      );
    }
  }

  async onSubmitTratamiento() {
    this.loading = true;
    // debugger;

    let tratamiento = new Tratamiento(
      this.tratamientoForm.value.tratamiento,
      this.tratamientoForm.value.observacion
    );
    console.log(tratamiento);

    const response = await this._pacienteServices.crearTratamientoPaciente(
      tratamiento
    );
    console.log(response);
    this.loading = false;
    if (response["status"] === "success" && response["code"] === "200") {
      console.log(response["message"]);
      this._notificacionService.showNotification(
        "success",
        "top",
        "right",
        response["message"]
      );
    } else {
      console.log(response["message"]);
      this._notificacionService.showNotification(
        "danger",
        "top",
        "right",
        response["message"]
      );
    }
  }

  async calcularIndices() {
    const idAntropometria = localStorage.getItem("idAntropometria");

    this.loading = true;
    const response = await this._pacienteServices.listarIndicesPaciente(
      idAntropometria
    );
    console.log(response);
    this.loading = false;
    if (response["status"] === "success" && response["code"] === "200") {
      // this._notificacionService.showNotification('success', 'top', 'right', response["message"]);
      this.indices = response["data"];
      console.log(this.indices);

      switch (this.indices.corpulencia) {
        case "Peso Insuficiente":
          this.imgCorpulencia = 1;
          break;
        case "Normopeso":
          this.imgCorpulencia = 2;
          break;
        case "Sobrepeso Grado I":
          this.imgCorpulencia = 3;
          break;
        case "Sobrepeso Grado II (Preobesisdad)":
          this.imgCorpulencia = 4;
          break;
        case "Obesidad Tipo I":
          this.imgCorpulencia = 5;
          break;
        case "Obesidad Tipo II":
          this.imgCorpulencia = 6;
          break;
        case "Obesidad Tipo III (Mórbida)":
          this.imgCorpulencia = 7;
          break;
        case "Obesidad Tipo IV (Extrema)":
          this.imgCorpulencia = 8;
          break;
        default:
          this.imgCorpulencia = 9;
          break;
      }
    } else {
      this._notificacionService.showNotification(
        "danger",
        "top",
        "right",
        response["message"]
      );

      console.log(response["message"]);
    }
  }

  async onSubmitControl(idPaciente) {

    this.loading = true;

    const response = await this._pacienteServices.crearControl(
      idPaciente, 'Nuevo Control'
    );

    this.loading = false;
    console.log(response);
    if (response["status"] === "success" && response["code"] === "200") {
      console.log(response["message"]);
      console.log(response["data"]);
      this.idControl = response["data"].id;
      localStorage.setItem("idControl", this.idControl);

      this._notificacionService.showNotification(
        "success",
        "top",
        "right",
        response["message"]
      );
    } else {
      console.log(response["message"]);
      this._notificacionService.showNotification(
        "danger",
        "top",
        "right",
        response["message"]
      );
    }
  }

  regresar() {
    this._router.navigate(["/pacientes"]);
  }
}
