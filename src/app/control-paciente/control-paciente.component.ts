import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { NotificacionService } from "app/services/notificacion.service";
import { PacientesService } from "app/services/pacientes.service";

@Component({
  selector: "app-control-paciente",
  templateUrl: "./control-paciente.component.html",
  styleUrls: ["./control-paciente.component.css"],
})
export class ControlPacienteComponent implements OnInit {
  public loading = false;
  public controles: any[] =[];
  public idPaciente: any;
  pacienteData: any;

  constructor(
    private _route: Router,
    private _router: ActivatedRoute,
    private _pacientesServices: PacientesService,
    private _notificacionService: NotificacionService
  ) {

    this.idPaciente = this._router.snapshot.paramMap.get('idPaciente');
  }

  ngOnInit(): void {
    this.verPaciente();
  }

  async verPaciente() {
    this.loading = true;
    const response = await this._pacientesServices.listarPacientePorDocumento(
      this.idPaciente
    );
    this.loading = false;
    if (response["status"] === "success" && response["code"] === "200") {
      // this._notificacionService.showNotification('success', 'top', 'right', response["message"]);
      this.controles = response["data"];
      console.log(this.controles);

      this.controles.forEach(e => {
        this.pacienteData = e.pacienteIdPaciente;
      });


    } else {
      this._notificacionService.showNotification(
        "danger",
        "top",
        "right",
        response["message"]
      );

      console.log(response["message"]);
    }
  }

  nuevoControl(){
    this._route.navigate(['nuevo-control', this.idPaciente]);
  }

  regresar(){
    this._route.navigate(['pacientes']);
  }
}
