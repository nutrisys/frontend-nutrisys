import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UsuarioService } from './usuario.service';
import { environment } from '../../environments/environment';
import { RegistroUsuario } from '../models/registroUsuario';
import { GeneralResponse } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class RegistroUsuariosService {

  _headers: any;
  headers: any;
  url = environment.api;
  token: string;

  constructor(
    private http: HttpClient,
    private _usuarioService: UsuarioService
  ) { }

  async registrarUsuario(usuario: RegistroUsuario) {
    debugger;
    this.token = await this._usuarioService.getToken();
    this.headers = new HttpHeaders()
      .set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Authorization', this.token);

    return await new Promise((resolve, reject) => {
      const json = JSON.stringify(usuario);
      let params = "?json=" + json;
      this.http.post(this.url + "usuario" + params, { headers: this.headers }).subscribe(
        (response: GeneralResponse) => {
          resolve(response);
        },
        (error: any) => {
          reject(error);
        }
      );
    }).catch((error: any) => {
      console.log(error);
      throw error;
    });
  }

  async crearUsuarios(usuario: RegistroUsuario) {
    this.token = await this._usuarioService.getToken();

    this.headers = new HttpHeaders()
      .set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Authorization', this.token);

    return await new Promise((resolve, reject) => {
      const json = JSON.stringify(usuario);

      let params = "?json=" + json;

      this.http.post(this.url + "usuario"+ params, null, { headers: this.headers }).subscribe(
        (response: GeneralResponse) => {
          resolve(response);
        },
        (error: any) => {
          reject(error);
        }
      );
    }).catch((error: any) => {
      console.log(error);
      throw error;
    });
  }

  async listarUsuarios() {
    this.token = await this._usuarioService.getToken();
    this.headers = new HttpHeaders()
      .set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Authorization', this.token);

    return await new Promise((resolve, reject) => {
      this.http.get(this.url + "usuario", { headers: this.headers }).subscribe(
        (response: GeneralResponse) => {
          resolve(response);
        },
        (error: any) => {
          reject(error);
        }
      );
    }).catch((error: any) => {
      console.log(error);
      throw error;
    });
  }

  
}
