import { Injectable } from "@angular/core";
import { environment } from "../../environments/environment";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { UsuarioService } from "./usuario.service";
import {
  DatosPersonales,
  Antropometria,
  Comorbilidad,
  ComorbilidadData,
  PerfilBioquimico,
  Tratamiento,
} from "../models/pacientes";
import { GeneralResponse } from "../interfaces/interfaces";

@Injectable({
  providedIn: "root",
})
export class PacientesService {
  _headers: any;
  headers: any;
  url = environment.api;
  token: string;

  constructor(
    private http: HttpClient,
    private _usuarioService: UsuarioService
  ) {}

  async crearDatosPersonalesPaciente(pacienteDatosPersonales: DatosPersonales) {
    // debugger;
    this.token = await this._usuarioService.getToken();

    this.headers = new HttpHeaders()
      .set("Content-Type", "application/x-www-form-urlencoded")
      .set("Authorization", this.token);

    return await new Promise((resolve, reject) => {
      const json = JSON.stringify(pacienteDatosPersonales);

      let params = "?json=" + json;

      this.http
        .post(this.url + "paciente" + params, null, { headers: this.headers })
        .subscribe(
          (response: GeneralResponse) => {
            resolve(response);
          },
          (error: any) => {
            reject(error);
          }
        );
    }).catch((error: any) => {
      console.log(error);
      throw error;
    });
  }

  async crearAntopometriaPaciente(antopometria: Antropometria) {
    debugger;
    this.token = await this._usuarioService.getToken();

    this.headers = new HttpHeaders()
      .set("Content-Type", "application/x-www-form-urlencoded")
      .set("Authorization", this.token);

    return await new Promise((resolve, reject) => {
      const json = JSON.stringify(antopometria);

      let params = "?json=" + json;

      this.http
        .post(this.url + "antropometria" + params, null, {
          headers: this.headers,
        })
        .subscribe(
          (response: GeneralResponse) => {
            resolve(response);
          },
          (error: any) => {
            reject(error);
          }
        );
    }).catch((error: any) => {
      console.log(error);
      throw error;
    });
  }

  async crearComorbilidadPaciente(comorbilidad: ComorbilidadData) {
    debugger;
    this.token = await this._usuarioService.getToken();

    this.headers = new HttpHeaders()
      .set("Content-Type", "application/x-www-form-urlencoded")
      .set("Authorization", this.token);

    return await new Promise((resolve, reject) => {
      const json = JSON.stringify(comorbilidad);

      let params = "?json=" + json;

      this.http
        .post(this.url + "comorbilidad" + params, null, {
          headers: this.headers,
        })
        .subscribe(
          (response: GeneralResponse) => {
            resolve(response);
          },
          (error: any) => {
            reject(error);
          }
        );
    }).catch((error: any) => {
      console.log(error);
      throw error;
    });
  }

  async crearPerfilBioquimicoPaciente(perfilBioquimico: PerfilBioquimico) {
    debugger;
    this.token = await this._usuarioService.getToken();

    this.headers = new HttpHeaders()
      .set("Content-Type", "application/x-www-form-urlencoded")
      .set("Authorization", this.token);

    return await new Promise((resolve, reject) => {
      const json = JSON.stringify(perfilBioquimico);

      let params = "?json=" + json;

      this.http
        .post(this.url + "perfilBioquimico" + params, null, {
          headers: this.headers,
        })
        .subscribe(
          (response: GeneralResponse) => {
            resolve(response);
          },
          (error: any) => {
            reject(error);
          }
        );
    }).catch((error: any) => {
      console.log(error);
      throw error;
    });
  }

  async crearTratamientoPaciente(tratamiento: Tratamiento) {
    debugger;
    this.token = await this._usuarioService.getToken();

    this.headers = new HttpHeaders()
      .set("Content-Type", "application/x-www-form-urlencoded")
      .set("Authorization", this.token);

    return await new Promise((resolve, reject) => {
      const json = JSON.stringify(tratamiento);

      let params = "?json=" + json;

      this.http
        .post(this.url + "intervencion" + params, null, {
          headers: this.headers,
        })
        .subscribe(
          (response: GeneralResponse) => {
            resolve(response);
          },
          (error: any) => {
            reject(error);
          }
        );
    }).catch((error: any) => {
      console.log(error);
      throw error;
    });
  }

  async listarPacientes() {
    this.token = await this._usuarioService.getToken();
    this.headers = new HttpHeaders()
      .set("Content-Type", "application/x-www-form-urlencoded")
      .set("Authorization", this.token);

    return await new Promise((resolve, reject) => {
      this.http
        .get(this.url + "paciente/1", { headers: this.headers })
        .subscribe(
          (response: GeneralResponse) => {
            resolve(response);
          },
          (error: any) => {
            reject(error);
          }
        );
    }).catch((error: any) => {
      console.log(error);
      throw error;
    });
  }

  async listarPacientePorDocumento(id: any) {
    this.token = await this._usuarioService.getToken();
    this.headers = new HttpHeaders()
      .set("Content-Type", "application/x-www-form-urlencoded")
      .set("Authorization", this.token);

    const params = "?idPaciente=" + id;

    return await new Promise((resolve, reject) => {
      this.http
        .get(this.url + "controlesPaciente"+ params, { headers: this.headers })
        .subscribe(
          (response: GeneralResponse) => {
            resolve(response);
          },
          (error: any) => {
            reject(error);
          }
        );
    }).catch((error: any) => {
      console.log(error);
      throw error;
    });
  }

  async listarIndicesPaciente(idAntropometria: any) {
    this.token = await this._usuarioService.getToken();
    this.headers = new HttpHeaders()
      .set("Content-Type", "application/x-www-form-urlencoded")
      .set("Authorization", this.token);

    let params = "?idAntropometria=" + idAntropometria;

    return await new Promise((resolve, reject) => {
      this.http
        .get(this.url + "indices/2" + params, { headers: this.headers })
        .subscribe(
          (response: GeneralResponse) => {
            resolve(response);
          },
          (error: any) => {
            reject(error);
          }
        );
    }).catch((error: any) => {
      console.log(error);
      throw error;
    });
  }

  async crearControl(idPaciente: any, observacion: any) {
    debugger;
    this.token = await this._usuarioService.getToken();

    this.headers = new HttpHeaders()
      .set("Content-Type", "application/x-www-form-urlencoded")
      .set("Authorization", this.token);

    return await new Promise((resolve, reject) => {

      let params = "?idPaciente=" + idPaciente + "?observacion=" + observacion;

      this.http
        .post(this.url + "control" + params, null, {
          headers: this.headers,
        })
        .subscribe(
          (response: GeneralResponse) => {
            resolve(response);
          },
          (error: any) => {
            reject(error);
          }
        );
    }).catch((error: any) => {
      console.log(error);
      throw error;
    });
  }
}
