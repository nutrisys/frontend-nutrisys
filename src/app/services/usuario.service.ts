import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { RegistroUsuario } from '../models/registroUsuario';
import { GeneralResponse } from '../interfaces/interfaces';
import { storage } from 'googleapis/build/src/apis/storage';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  identity: any;
  _headers: any;
  headers: any;
  url = environment.api;
  token: string;

  constructor(
    private http: HttpClient,
    private router: Router
  ) {
    console.log('[Usuario services is ready]...');
  }

  async iniciarSesion(usuario: any, gettoken = null) {
    this._headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');

    return await new Promise((resolve, reject) => {
      if (gettoken != null) {
        usuario.gettoken = "true";
      }
      const json = JSON.stringify(usuario);
      const params = "?json=" + json;
      this.http.post(this.url + "login" + params, { headers: this._headers }).subscribe(
        (response: any) => {
          resolve(response);
        },
        (error: any) => {
          reject(error);
        }
      );
    }).catch((error: any) => {
      console.log(error);
      throw error;
    });
  }

  // async expirarToken() {
  // 	this.token = await this.getToken();
  // 	this.headers = new HttpHeaders()
  // 		.set("Content-Type", "application/x-www-form-urlencoded")
  // 		.set("Authorization", this.token);

  // 	return await new Promise((resolve, reject) => {
  // 		this.http.get(this.url + "/validarToken", { headers: this.headers }).subscribe(
  // 			(response: any) => {
  // 				resolve(response);
  // 			},
  // 			(error: any) => {
  // 				reject(error);
  // 			}
  // 		);
  // 	}).catch((error: any) => {
  // 		console.log(error);
  // 		throw error;
  // 	});
  // }

  getIdentity() {
    const identity = JSON.parse(localStorage.getItem('identity'));

    if (identity && identity !== "undefined") this.identity = identity;
    else this.identity = null;

    return this.identity;
  }

  async getToken(): Promise<string> {
    const token = localStorage.getItem("token");

    if (token && token !== "undefined") this.token = token;
    else this.token = null;

    return await this.token;
  }

  isToken() {
    const isToken = localStorage.getItem("token");
    return isToken ? true : false;
  }

  cerrarSesion(){
    localStorage.clear();
    this.router.navigate(['iniciar-sesion'])
  }

}
