import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UsuarioService } from '../services/usuario.service';
import { RegistroUsuario } from '../models/registroUsuario';
import { RegistroUsuariosService } from '../services/registro-usuarios.service';
import { async } from '@angular/core/testing';
import { NotificacionService } from '../services/notificacion.service';

@Component({
  selector: 'app-registrar-usuarios',
  templateUrl: './registrar-usuarios.component.html',
  styleUrls: ['./registrar-usuarios.component.css']
})
export class RegistrarUsuariosComponent implements OnInit {

  usuarioForm: FormGroup;
  loading = false;;


  constructor(
    private _router: Router,
    private _registroUsuarioService: RegistroUsuariosService,
    private _notificacionService: NotificacionService
  ) { }

  ngOnInit(): void {

    this.usuarioForm = new FormGroup({
      rol: new FormControl(null, Validators.required),
      nombres: new FormControl(null, Validators.required),
      apellidos: new FormControl(null, Validators.required),
      documento: new FormControl(null, Validators.required),
      regsitroProfesional: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required),
    });

  }

  async onSubmitCrearUsuario() {

    this.loading = true;
    //debugger;

    let usuario = new RegistroUsuario(
      this.usuarioForm.value.rol,
      this.usuarioForm.value.nombres,
      this.usuarioForm.value.apellidos,
      this.usuarioForm.value.documento,
      this.usuarioForm.value.regsitroProfesional,
      this.usuarioForm.value.password,
    ); //console.log(cliente);

    const response = await this._registroUsuarioService.crearUsuarios(usuario);
    console.log(response);
    this.loading = false;
    if (response["status"] === "success" && response["code"] === "200") {
      console.log(response["message"]);
      this._notificacionService.showNotification('success', 'top', 'right', response["message"]);
    } else {
      console.log(response["message"]);
      this._notificacionService.showNotification('danger', 'top', 'right', response["message"]);

    }
  }


  regresar(){
    this._router.navigate(['/usuarios']);
  }


}
