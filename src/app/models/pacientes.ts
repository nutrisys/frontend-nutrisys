export class DatosPersonales {
    constructor(
        public documento: string,
        public nombres: string,
        public apellidos: string,
        public genero: string,
        public email: string,
        public direccion: string,
        public telefono: string,
        public fechaNacimiento: string
        // public edad: string,
    ) { }
}

export class Antropometria {
    constructor(
        public altura: string,
        public peso: string,
        public radioCaderaCintura: string,
        public circunferenciaMediaCuello: string,
        public circunferenciaMediaAbdomen: string,
        public circunferenciaMediaCadera: string,
        public idControl: string
    ) { }
}


export class Comorbilidad {
    constructor(
        public hipertension: string,
        public diabetes: string,
        public infartoMediocardioPrevio: string,
        public enfrenalCronica: string,
        public enfRenalCronicaDialisis: string,
        public enfCerebrovascular: string,
        public epoc: string,
        public tabaquismo: string,
        public sahos: string,
        public vih: string,
        public neoplasiaActiva: string,
        public enfAutoinmune: string
    ) { }
}

export class ComorbilidadData {
    constructor(
        public descripcion: string,
        public perfilBioquimico: string,
        public idControl: string
    ) { }
}

export class PerfilBioquimico {
    constructor(
        public glucemia: string,
        public insulina: string,
        public homair: string,
        public leptina: string,
        public idControl:string
    ) { }
}

export class Indices {
    constructor(
        public imc: string,
        public imgrasa: string,
        public masagrasacorporal: string,
        public imlibredegrasa: string,
        public masalibredegrasa: string,
        public masamusculoesquelitica: string
    ) { }
}

export class Tratamiento {
    constructor(
        public descripcion: string,
        public observacion: string
    ) { }
}