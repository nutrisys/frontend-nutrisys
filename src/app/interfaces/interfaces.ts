export interface GeneralResponse {
	status: string;
	code: number;
	message: string;
	data: any[];
}
